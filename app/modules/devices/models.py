# encoding: utf-8
"""
Device database models
--------------------
"""

from app.extensions import db


class DeviceDict(db.Model):
    """
    Device-Dict database model.
    """

    id = db.Column(
        db.Integer,
        primary_key=True, autoincrement=True)  # pylint: disable=invalid-name

    type = db.Column(db.String(length=50))
    value = db.Column(db.String(length=50))
    company = db.Column(db.String(length=50))


class DeviceRecord(db.Model):
    """
    Device-record database model.
    """
    id = db.Column(
        db.Integer,
        primary_key=True, autoincrement=True)  # pylint: disable=invalid-name
    type = db.Column(db.String(length=50))

    accountant_code = db.Column(
        db.String(
            length=50),
        default='',
        nullable=True)

    accountant_name = db.Column(
        db.String(
            length=50),
        default='',
        nullable=False)
    bill_name = db.Column(db.String(length=50))
    code = db.Column(db.String(length=50))
    description = db.Column(db.String(length=50))
    unit = db.Column(db.String(length=10))
    month = db.Column(db.String(length=20))
    remark = db.Column(db.String(length=200))

    business_date = db.Column(db.Date())
    real_date = db.Column(db.Date())


    in_date = db.Column(db.Date())
    in_real_date = db.Column(db.Date())
    in_amount = db.Column(db.Integer())
    in_unit_fee = db.Column(db.Float())
    in_fee = db.Column(db.Float())
    in_auth_date = db.Column(db.Date())
    in_provider = db.Column(db.String(length=50))
    in_tax_fee = db.Column(db.String(length=50))
    in_tax_amount = db.Column(db.String(length=50))
    in_price_tax = db.Column(db.String(length=50))
    in_tax_date = db.Column(db.Date())
    in_tax_no = db.Column(db.String(length=50))
    in_client = db.Column(db.String(length=50))
    in_remark = db.Column(db.String(length=200))

    sale_date = db.Column(db.Date())
    sale_real_date = db.Column(db.Date())
    sale_amount = db.Column(db.Integer())
    sale_unit_fee = db.Column(db.Float())
    sale_fee = db.Column(db.Float())
    sale_people = db.Column(db.String(length=200))
    sale_client = db.Column(db.String(length=200))
    sale_tax_fee = db.Column(db.String(length=50))
    sale_tax_amount = db.Column(db.String(length=50))
    sale_price_tax = db.Column(db.String(length=50))
    sale_tax_no = db.Column(db.String(length=50))
    sale_remark = db.Column(db.String(length=200))

    balance_date = db.Column(db.Date())
    balance_amount = db.Column(db.Integer())
    balance_unit_fee = db.Column(db.Float())
    balance_fee = db.Column(db.Float())

    balance_buy_date = db.Column(db.Date())

    company = db.Column(db.String(length=50))


class Device(db.Model):
    """
    Device database model.
    """

    id = db.Column(
        db.Integer,
        primary_key=True, autoincrement=True)  # pylint: disable=invalid-name
    code = db.Column(db.String(length=50))
    accountant_name = db.Column(
        db.String(
            length=50),
        default='',
        nullable=True)
    bill_name = db.Column(db.String(length=50))
    description = db.Column(db.String(length=50))
    unit = db.Column(db.String(length=10))

    company = db.Column(db.String(length=50))
