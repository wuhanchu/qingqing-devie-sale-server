# encoding: utf-8
"""
Input arguments (Parameters) for Device resources RESTful API
-----------------------------------------------------------
"""
from app.extensions.api.parameters import PaginationParameters
from flask_restplus_patched import PostFormParameters, Parameters
from flask_marshmallow import base_fields

from . import schemas
from .models import Device


class DeviceParameters(PostFormParameters):

    class Meta(schemas.DeviceSchema.Meta):
        # This is not supported yet:
        # https://github.com/marshmallow-code/marshmallow/issues/344
        required = (

        )


class DeviceRecordParameters(PostFormParameters):

    class Meta(schemas.DeviceRecordSchema.Meta):
        # This is not supported yet:
        # https://github.com/marshmallow-code/marshmallow/issues/344
        required = (

        )


class QueryParameters(PaginationParameters):
    code = base_fields.String()
    business_date_begin = base_fields.Date()
    business_date_end = base_fields.Date()
    company = base_fields.String()


class DeviceDictParameters(PostFormParameters):

    class Meta(schemas.DeviceDictSchema.Meta):
        # This is not supported yet:
        # https://github.com/marshmallow-code/marshmallow/issues/344
        required = (

        )
