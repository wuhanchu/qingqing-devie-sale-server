# encoding: utf-8
# pylint: disable=bad-continuation
"""
RESTful API Device resources
--------------------------
"""
import copy
import logging

from flask import json, jsonify

from app.extensions import db
from app.extensions.api import Namespace
from app.modules.devices.parameters import QueryParameters
from app.modules.devices.schemas import DeviceSchema, DeviceDictSchema, DeviceRecordSchema
from app.modules.users import permissions
from flask_restplus_patched import Resource
from flask_restplus_patched._http import HTTPStatus
from flask_restplus_patched.model import QueryResult
from . import parameters, schemas
from .models import Device, DeviceRecord, DeviceDict

log = logging.getLogger(__name__)  # pylint: disable=invalid-name
api = Namespace(
    'devices',
    description="Devices")  # pylint: disable=invalid-name


@api.route('/')
@api.login_required(oauth_scopes=['devices:read'])
class Devices(Resource):
    """
    Manipulations with devices.
    """

    @api.parameters(QueryParameters())
    @api.response(QueryResult())
    def get(self, args):
        """
        List of devices.

        Returns a list of devices starting from ``offset`` limited by ``limit``
        parameter.
        """
        query = Device.query.filter(
            Device.company == args.get('company'))
        device_list = DeviceSchema().dump(
            query.offset(
                args['offset']).limit(
                args['limit']).all(), True).data
        return {
            'data': device_list, 'total': query.count()}

    @api.login_required(oauth_scopes=['devices:write'])
    @api.parameters(parameters.DeviceParameters())
    @api.response(schemas.DeviceSchema())
    @api.response(code=HTTPStatus.CONFLICT)
    def post(self, args):
        """
        Create a new device.
        """
        with api.commit_or_abort(
            db.session,
            default_error_message="Failed to create a new device"
        ):
            device = Device(**args)
            db.session.add(device)

        return device


@api.route('/<int:device_id>')
@api.login_required(oauth_scopes=['devices:read'])
@api.response(
    code=HTTPStatus.NOT_FOUND,
    description="Device not found.",
)
@api.resolve_object_by_model(Device, 'device')
class DeviceByID(Resource):
    """
    Manipulations with a specific device.
    """

    @api.permission_required(
        permissions.OwnerRolePermission,
        kwargs_on_request=lambda kwargs: {'obj': kwargs['device']}
    )
    @api.response(schemas.DeviceSchema())
    def get(self, device):
        """
        Get device details by ID.
        """
        return device

    @api.login_required(oauth_scopes=['devices:write'])
    @api.permission_required(
        permissions.OwnerRolePermission,
        kwargs_on_request=lambda kwargs: {'obj': kwargs['device']}
    )
    @api.permission_required(permissions.WriteAccessPermission())
    @api.parameters(parameters.DeviceParameters())
    @api.response(schemas.DeviceSchema())
    @api.response(code=HTTPStatus.CONFLICT)
    def patch(self, args, device):
        """
        Patch device details by ID.
        """
        with api.commit_or_abort(
            db.session,
            default_error_message="Failed to update device details."
        ):
            for key in args:
                setattr(device, key, args.get(key))

            db.session.merge(device)
        return device

    @api.login_required(oauth_scopes=['devices:write'])
    @api.permission_required(
        permissions.OwnerRolePermission,
        kwargs_on_request=lambda kwargs: {'obj': kwargs['device']}
    )
    @api.permission_required(permissions.WriteAccessPermission())
    @api.response(code=HTTPStatus.CONFLICT)
    @api.response(code=HTTPStatus.NO_CONTENT)
    def delete(self, device):
        """
        Delete a device by ID.
        """
        with api.commit_or_abort(
            db.session,
            default_error_message="Failed to delete the device."
        ):
            db.session.delete(device)
        return None


def _record_save(record):
    if record.type == '本期销售':
        record.in_date = None
        record.in_real_date = None
        record.in_amount = None
        record.in_unit_fee = None
        record.in_fee = None
        record.in_auth_date = None
        record.in_provider = None
        record.in_tax_fee = None
        record.in_tax_amount = None
        record.in_price_tax = None
        record.in_tax_date = None
        record.in_tax_no = None
        record.in_client = None
        record.in_remark = None
    elif record.type == '本期购进':
        record.sale_date = None
        record.sale_real_date = None
        record.sale_amount = None
        record.sale_unit_fee = None
        record.sale_fee = None
        record.sale_people = None
        record.sale_client = None
        record.sale_tax_fee = None
        record.sale_tax_amount = None
        record.sale_price_tax = None
        record.sale_tax_no = None
        record.sale_remark = None
    if not record.in_auth_date:
        record.in_auth_date = None

    #  reset the data
    switch = {
        '本期购进': record.in_date,
        '本期销售': record.sale_date
    }
    record.business_date = switch.get(record.type, record.balance_date)
    switch = {
        '本期购进': record.in_real_date,
        '本期销售': record.sale_real_date
    }
    record.real_date = switch.get(
        record.type, record.balance_date)

    return record


def _merge_record(last_record, record):
    record = copy.copy(record)

    # set current data
    if record.type == '本期销售':
        # record.sale_unit_fee = (last_record.balance_unit_fee if last_record else 0)
        # record.sale_fee = record.sale_unit_fee * record.sale_amount

        record.balance_date = record.sale_date
        record.balance_amount = (
            last_record.balance_amount if last_record else 0) - record.sale_amount

        # record.balance_unit_fee = record.sale_unit_fee
        # record.balance_fee = record.balance_unit_fee * record.balance_amount

    elif record.type == '本期购进':
        record.balance_date = record.in_date
        record.balance_amount = (
            last_record.balance_amount if last_record else 0) + record.in_amount

        # record.balance_unit_fee = (
        #     (last_record.balance_fee if last_record else 0) + record.in_fee) / record.balance_amount
        # record.balance_fee = record.balance_unit_fee * record.balance_amount

    return record


def _get_current_inventory(record):
    record_list = DeviceRecord.query.filter(
        DeviceRecord.code == record.code,
        DeviceRecord.business_date < record.business_date).order_by(
        DeviceRecord.business_date.desc(),
        DeviceRecord.id.desc())

    result = None
    for record in record_list:
        result = _merge_record(result, record)

    return result


def _get_record_list(device_record_list):
    # 优化所有的库存数据
    record_map = {}
    result = []
    for record in device_record_list:
        last_record = record_map.get(record.code)
        if last_record is None:
            last_record = _get_current_inventory(record)
            record_map[record.code] = last_record
        record = _merge_record(last_record, record)
        record_map[record.code] = record
        result.append(record)
    return result


@api.route('/records/statistics')
@api.response(
    code=HTTPStatus.NOT_FOUND,
    description="statistics not found.",
)
class RecordStatistics(Resource):

    @api.parameters(QueryParameters())
    def get(self, args):
        # 查询需要的数据
        query = DeviceRecord.query
        query = query.filter(
            DeviceRecord.company == args.get('company')).filter(
            DeviceRecord.real_date.isnot(None))
        business_date_begin = args.get('business_date_begin')
        if args.get('business_date_end'):
            query = query.filter(
                DeviceRecord.real_date <= args.get('business_date_end'))
        if args.get('code'):
            query = query.filter(
                DeviceRecord.code.like(args.get('code') + '%'))

        # 查询需要的数据
        device_record_list = query.order_by(
            DeviceRecord.code.asc(),
            DeviceRecord.real_date.asc()).all()

        # 排序数据
        device_record_list = _get_record_list(device_record_list)

        # 优化所有的库存数据
        result_record = None
        result = []
        result_map = {}
        last_result_map = {}
        for record in device_record_list:
            if result_record and ((result_record.get('code') != record.code) or (
                    result_record.get('month') != record.real_date.strftime("%Y-%m"))):

                # convert
                last_result_record = last_result_map.get(
                    result_record.get('code'))
                result_record = self.__merge_data(
                    result_record,
                    last_result_record)

                # append
                last_result_map[
                    result_record.get('code')] = result_map.pop(
                    result_record.get('code'))

                if not business_date_begin or result_record.get(
                        'real_date') >= business_date_begin:
                    result.append(result_record)

                result_record = None

            if result_record is None:
                result_record = {
                    'code': record.code,
                    'accountant_name': record.accountant_name,
                    'sale_amount': 0,
                    'sale_fee': 0,
                    'in_amount': 0,
                    'in_fee': 0,
                    'real_date': record.real_date,
                    'month': record.real_date.strftime("%Y-%m")
                }

            if record.type == '本期销售':
                result_record['sale_amount'] += record.sale_amount
                # result_record['sale_fee'] += round(record.sale_fee, 2)

            elif record.type == '本期购进':
                result_record['in_amount'] += record.in_amount
                result_record['in_fee'] += record.in_fee

            result_map[result_record.get('code')] = result_record

        for key, result_record in result_map.items():
            last_result_record = last_result_map.get(result_record.get('code'))
            if not business_date_begin or result_record.get(
                    'real_date') >= business_date_begin:
                result.append(
                    self.__merge_data(
                        result_record,
                        last_result_record))

        return jsonify({'data': result})

    def __merge_data(self, result_record, last_result_record):
        result_record['in_unit_fee'] = round(
            result_record['in_fee'] / result_record['in_amount'],
            2) if result_record['in_amount'] > 0 else 0

        result_record['sale_unit_fee'] = round(
            ((last_result_record['balance_fee']if last_result_record else 0) +
             result_record['in_fee']) /
            (
                (last_result_record['balance_amount'] if last_result_record else 0) +
                result_record['in_amount']),
            2)

        # result_record['sale_unit_fee'] = round(
        #     result_record['sale_fee'] / result_record['sale_amount'],
        #     2) if result_record['sale_amount'] > 0 else 0

        result_record['sale_fee'] += result_record['sale_unit_fee'] * \
            result_record['sale_amount']

        result_record['balance_amount'] = round(
            (last_result_record.get('balance_amount') if last_result_record else 0) +
            result_record.get('in_amount') -
            result_record.get('sale_amount'),
            2)
        result_record['balance_fee'] = round(
            (last_result_record.get('balance_fee') if last_result_record else 0) +
            result_record.get('in_fee') -
            result_record.get('sale_fee'),
            2)
        result_record['balance_unit_fee'] = round(
            result_record.get('balance_fee') /
            result_record.get('balance_amount'),
            2) if result_record.get('balance_amount') > 0 else 0

        result_record['last_balance_amount'] = last_result_record[
            'balance_amount'] if last_result_record else 0
        result_record['last_balance_fee'] = last_result_record[
            'balance_fee'] if last_result_record else 0
        result_record['last_balance_unit_fee'] = last_result_record[
            'balance_unit_fee'] if last_result_record else 0

        return result_record


@api.route('/records/')
@api.login_required(oauth_scopes=['devices:read'])
@api.response(

    code=HTTPStatus.NOT_FOUND,
    description="Device not found.",
)
class DeviceRecords(Resource):
    """
    Manipulations with records of a specific device.
    """

    @api.parameters(QueryParameters())
    @api.response(QueryResult())
    def get(self, args):
        """
        Get device records by device ID.
        """
        # 查询需要的数据
        query = DeviceRecord.query
        query = query.filter(
            DeviceRecord.company == args.get('company'))
        if args.get('business_date_begin'):
            query = query.filter(
                DeviceRecord.business_date >= args.get('business_date_begin'))
        if args.get('business_date_end'):
            query = query.filter(
                DeviceRecord.business_date <= args.get('business_date_end'))
        if args.get('code'):
            query = query.filter(
                DeviceRecord.code.like(args.get('code') + '%'))

        device_record_list = query.order_by(
            DeviceRecord.code.asc(),
            DeviceRecord.business_date.desc()).offset(
            args['offset']).limit(
            args['limit']).all()

        # 排序数据
        device_record_list = sorted(
            device_record_list,
            key=lambda record: (record.code, record.business_date))

        device_record_list = _get_record_list(device_record_list)
        device_record_list = DeviceRecordSchema().dump(device_record_list, True).data

        # 返回
        return {
            'data': device_record_list, 'total': query.count()}

    @api.login_required(oauth_scopes=['devices:write'])
    @api.permission_required(permissions.WriteAccessPermission())
    @api.parameters(parameters.DeviceRecordParameters())
    @api.response(schemas.DeviceRecordSchema())
    @api.response(code=HTTPStatus.CONFLICT)
    def post(self, args):
        """
        Add a new record to a device.
        """
        with api.commit_or_abort(
            db.session,
            default_error_message="Failed to create a new device record"
        ):
            device_record = DeviceRecord(**args)
            device_record = _record_save(device_record)

            #  save
            db.session.add(device_record)

        return device_record


@api.route('/records/<int:record_id>')
@api.response(
    code=HTTPStatus.NOT_FOUND,
    description="Device or record not found.",
)
@api.resolve_object_by_model(DeviceRecord, 'record')
class DeviceRecordByID(Resource):
    """
    Manipulations with a specific device record.
    """

    @api.response(code=HTTPStatus.CONFLICT)
    def delete(self, record):
        """
        Remove a record from a device.
        """
        with api.commit_or_abort(
            db.session,
            default_error_message="Failed to delete device details."
        ):
            db.session.delete(record)
        return None

    @api.parameters(parameters.DeviceRecordParameters())
    @api.response(schemas.DeviceRecordSchema())
    @api.response(code=HTTPStatus.CONFLICT)
    def patch(self, args, record):
        """
        Patch dict details by ID.
        """
        with api.commit_or_abort(
            db.session,
            default_error_message="Failed to update dict details."
        ):
            for key in args:
                setattr(record, key, args.get(key))

            record = _record_save(record)
            # set th business date

            # save
            db.session.merge(record)
        return record


@api.route('/dicts/')
@api.login_required(oauth_scopes=['devices:read'])
class DeviceDicts(Resource):
    """
    Manipulations with dicts of a specific device.
    """

    @api.parameters(QueryParameters())
    @api.response(QueryResult())
    def get(self, args, ):
        query = DeviceDict.query.filter(
            DeviceDict.company == args.get('company'))
        device_dict_list = query.offset(
            args['offset']).limit(
            args['limit']).all()
        return {
            'data': DeviceDictSchema().dump(
                device_dict_list,
                True).data,
            'total': query.count()}

    @api.parameters(parameters.DeviceDictParameters())
    @api.response(schemas.DeviceDictSchema())
    @api.response(code=HTTPStatus.CONFLICT)
    def post(self, args):
        """
        Add a new record to a device.
        """
        with api.commit_or_abort(
            db.session,
            default_error_message="Failed to create a new device"
        ):
            device_dict = DeviceDict(**args)
            db.session.add(device_dict)

        return device_dict


@api.route('/dicts/<int:dict_id>')
@api.login_required(oauth_scopes=['devices:read'])
@api.response(
    code=HTTPStatus.NOT_FOUND,
    description="Dict not found.",
)
@api.resolve_object_by_model(DeviceDict, 'dict')
class DeviceDictByID(Resource):
    """
    Manipulations with a specific dict.
    """

    @api.permission_required(
        permissions.OwnerRolePermission,
        kwargs_on_request=lambda kwargs: {'obj': kwargs['dict']}
    )
    @api.response(schemas.DeviceDictSchema())
    def get(self, dict):
        """
        Get dict details by ID.
        """
        return dict

    @api.login_required(oauth_scopes=['devices:write'])
    @api.parameters(parameters.DeviceDictParameters())
    @api.response(schemas.DeviceDictSchema())
    @api.response(code=HTTPStatus.CONFLICT)
    def patch(self, args, dict):
        """
        Patch dict details by ID.
        """
        with api.commit_or_abort(
            db.session,
            default_error_message="Failed to update dict details."
        ):
            for key in args:
                setattr(dict, key, args.get(key))

            db.session.merge(dict)
        return dict

    @api.login_required(oauth_scopes=['devices:write'])
    @api.permission_required(permissions.WriteAccessPermission())
    @api.response(code=HTTPStatus.CONFLICT)
    @api.response(code=HTTPStatus.NO_CONTENT)
    def delete(self, dict):
        """
        Delete a dict by ID.
        """
        with api.commit_or_abort(
            db.session,
            default_error_message="Failed to delete the dict."
        ):
            db.session.delete(dict)
        return None
